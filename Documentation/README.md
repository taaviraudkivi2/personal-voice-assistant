**Version 1.0.0**

#Python voice assistant Jessica

## Overview

This program was created as a Computer Science of University of Tartu BSc subject _Programming_ project. It is a hard coded programmer themed voice assistant for asking simple questions and running scripts. 

##Features

Voice assistant Jessica answers to simple questions like
- "Hello Jessica", "How are you Jessica" etc  

It can run simple helpful scripts by voice commands
- Open Google and Google Tab (right now works only, if google is on specific path)
- Show avaivable Information Technology jobs displayed in MS Exel
- Create a new project folder and init git there 

Voice assistant has simple GUI, with navigation and list of available commands

Also there is a random password generator accessible form GUI. 

##Bugs

When voice assistant is activated any mouse click would crash the program

## Installation guide 

- pip install SpeechRecognition
- pip install pyttsx3
- pip install beautifulsoup4
- pip install requests

Depending on pip version, _pip3_ should be used 

## Contributors

- Andre Viibur <viiburandre@gmail.com>
- Taavi Raudkivi <taaviraudkivi2@gmail.com>

### Licence and copyright

 © Andre Viibur, Taavi Raudkivi ; University of Tartu 