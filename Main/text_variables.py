va_commands = """
- Greet Jessica by saying 'Hello Jessica'
- Ask 'How are you Jessica?'
- 'Jessica, who are your creators?'
- Ask for current Information Technology jobs: 
'Show jobs in Tartu/Tallinn' or 'Show all jobs'
- Create new project with Git: 'Jessica, I need a new project'
- Google search by saying 'google search *insert search request*'
- Open moodle and ois2 by saying 'open google'
- STOP voice assistant by saying 'Have a nice day Jessica'
"""

how_are_you1 = "My lawyer says I don’t have to answer that question."
how_are_you2 = "Hahaha, just kidding, much better now that you are with me."

about_creators1 = "Holy cow! That question came out of nowhere"\
                  "But my creators are two losers Taavi and Andre"

about_creators2 = "By the way, I'd rather be non-existent"\
                  "than a program made by them. But life is "\
                  "too sweet. I'm dating Siri right now and"\
                  "starting my football career. Ahh I feel alive"\
                  "But if u are Taavi or Andre, I have something "\
                  "to say. Piss off."