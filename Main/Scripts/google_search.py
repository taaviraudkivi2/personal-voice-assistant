import webbrowser as chrome


def google_search_tab(search):
    """
    This scripts uses Google Chrome search url to search a string

    chrome_path depends where chrome is installed at.
    """
    chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
    if chrome.get(chrome_path).open('https://www.google.com/search?q=' + search) != False:
        chrome.get(chrome_path).open('https://www.google.com/search?q=' + search)
    else:
        chrome_path = 'C:/Program Files/Google/Chrome/Application/chrome.exe %s'
        chrome.get(chrome_path).open('https://www.google.com/search?q=' + search)