import random
import string


def random_password(length):
    """T
    his script creates a random password using string library
    """
    letters = string.ascii_letters
    numbers = string.digits
    combo = letters + numbers  # adds everything to a long list

    random.shuffle(list(combo))

    short_password = random.choices(combo, k=length)  # chooses random letters from the list
    short_password = ''.join(short_password)
    return short_password
