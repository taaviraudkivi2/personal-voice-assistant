import webbrowser as chrome


def google_tab_opener():
    """
    This scripts opens two Google Chrome tabs Moodle and Õis2

    chrome_path depends where chrome is installed at.
    """
    chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
    if chrome.get(chrome_path).open("moodle.ut.ee") != False:
        chrome.get(chrome_path).open("moodle.ut.ee")
        chrome.get(chrome_path).open("ois2.ut.ee")
    else:
        chrome_path = 'C:/Program Files/Google/Chrome/Application/chrome.exe %s'
        chrome.get(chrome_path).open("moodle.ut.ee")
        chrome.get(chrome_path).open("ois2.ut.ee")