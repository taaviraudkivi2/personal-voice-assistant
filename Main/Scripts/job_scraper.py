"""
This script is for searching jobs in either Tallin, Tartu or both
Displays it as an excel table
"""

import os
import csv
import requests
from bs4 import BeautifulSoup


def find_all(tag, tag_class, soup):
    """
    Finds specific class html code and returns its text
    """
    result = []
    for text in soup.find_all(tag, {"class": tag_class}):
        result.append(text.get_text())
    return result


def set_url(job, location):
    r = "https://cv.ee/search?limit=1000&offset=0&categories%5B0%5D={}&towns%5B0%5D={}"
    return r.format(job, location)


def all_offerings(names, companies, locations, publication):
    offering = []
    i = 0
    for j in locations:
        offering.append([names[i], companies[i], j.replace("\xa0—\xa0", u""), publication[i]])
        i += 1
    return offering


def find_job_offerings(url):
    """
    Gets information from website and returns it in list
    """
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")

    job_names = find_all("span", "jsx-1471379408 vacancy-item__title", soup)
    companies_raw = find_all("div", "jsx-1471379408 vacancy-item__info-main", soup)
    locations = find_all("span", "jsx-1471379408 vacancy-item__locations", soup)
    publications = find_all("span", "jsx-1471379408 secondary-text", soup)

    companies = []
    for j in companies_raw:  # Remove tags and other info
        companies.append(j.split("\xa0—\xa0")[0])

    offerings = all_offerings(job_names, companies, locations, publications)
    return offerings


def display_job_offerings(offerings):
    with open("job_offerings.csv", "w", newline="", encoding="UTF-8-sig") as job_offerings:
        job_writer = csv.writer(job_offerings)
        job_writer.writerow(["Job", "Firm", "Location","Posted"])
        job_writer.writerow(["", "", ""])
        for i in offerings:
            job_writer.writerow(i)

    os.startfile("job_offerings.csv")