"""
This script initiates a git directory
"""

import os

def cmd_commands(project_name):
    """
    Creates a new project folder in current user directory
    and inits git there
    """
    path = 'C:/Users/' + os.getlogin() + "/" + project_name
    if not os.path.exists(path):
        os.makedirs(path)
    else:
        return cmd_commands(project_name+"1")
    os.system("git init {}".format(path))
