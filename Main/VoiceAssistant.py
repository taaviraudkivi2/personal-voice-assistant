from Gui import Gui
from text_variables import *


class VoiceAssistant(Gui):
    def __init__(self, command_list, how_are_you11,  how_are_you22, about_creators11, about_creators22):
        super().__init__(command_list=command_list, how_are_you1=how_are_you11, how_are_you2=how_are_you22,
                         about_creators1=about_creators11, about_creators2=about_creators22)

    def run_gui(self):
        try:
            self.create_frontend('THIS IS JESSICA')
        except:  # Except doesn't let the program crash
            pass


if __name__ == "__main__":
    va = VoiceAssistant(va_commands, how_are_you1, how_are_you2,
                        about_creators1, about_creators2)
    va.run_gui()