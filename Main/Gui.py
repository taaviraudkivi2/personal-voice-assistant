import speech_recognition as sr
import tkinter as tk
from time import sleep
from Voice import Voice


class Gui(Voice):
    def __init__(self, command_list, how_are_you1, how_are_you2, about_creators1, about_creators2):
        super().__init__(how_are_you1=how_are_you1, how_are_you2=how_are_you2,
                         about_creators1=about_creators1, about_creators2=about_creators2)
        self.command_list = command_list
        self.bg1 = "#141A32"  # main background color
        self.bg2 = "#c93853"  # VA button background color
        self.bg3 = "#CF6679"  # other button bg color
        self.fg1 = "white"  # text color
        self.font1 = "Times 15 bold"  # text size for new windows
        self.font2 = "Times 20 bold"  # main text
        self.font3 = "Times 40 bold"  # main window Label

    def run_voice(self):
        """
        Runs voice input and all voice output methods
        """
        while True:
            try:
                self.voice_in()
                self.say_hello()
                self.say_how_are_you()
                self.say_creators()
                self.open_google()
                self.run_job_scraper()
                self.search()
                self.git_init()
                if self.kill_app():
                    break
            except sr.UnknownValueError:
                print("I dont understand you or you are not speaking!")
            except sr.RequestError as error:
                print("Failed api request: {}".format(error))
                continue
        sleep(1)
        self.voice_output("You're still here? It's over. Close the fucking program!")

    def create_frontend(self, display_text):
        self.window = tk.Tk()
        self.window.title('Voice Assistant GUI')
        self.window.configure(bg=self.bg1)
        tk.Label(self.window, text=display_text, height=2, bg=self.bg1, fg=self.fg1, font=self.font3).pack()

        tk.Button(master=self.window, command=lambda: self.run_voice(), height=2, width=20,
                  bg=self.bg2, fg=self.fg1, font=self.font2, borderwidth=2,
                  text='Voice Assistant Jessica').pack()

        tk.Button(master=self.window, command=lambda: self.commands(), height=2, width=20,
                  bg=self.bg3, fg=self.fg1, font=self.font2, borderwidth=2,
                  text='Commands for Jessica').pack()

        tk.Button(master=self.window, command=lambda: self.generate_password(), height=2, width=20,
                  bg=self.bg3, fg=self.fg1, font=self.font2, borderwidth=2,
                  text='Generate password').pack()

        tk.Button(master=self.window, command=lambda: self.m_list(), height=2, width=20,
                  bg=self.bg3, fg=self.fg1, font=self.font2, borderwidth=2,
                  text='Microphone selection').pack()

        tk.Button(master=self.window, command=lambda: self.window.destroy(), height=2, width=20,
                  bg=self.bg3, fg=self.fg1, font=self.font2, borderwidth=2,
                  text='Close program').pack()

        self.window.mainloop()  # initializes the loop

    def commands(self):
        newWindow = tk.Toplevel()
        newWindow.title('VA command list')
        newWindow.configure(bg=self.bg1)
        label = tk.Label(newWindow, bg=self.bg1, fg=self.fg1, font=self.font1, text=self.command_list).pack()
        newWindow.mainloop()

    def generate_password(self):
        newWindow = tk.Toplevel()
        newWindow.title('Password Generator')
        newWindow.configure(bg=self.bg1)
        self.generated = tk.Entry(newWindow)  # Entry widget for password output, packs it in new_entry_box()

        tk.Label(newWindow, bg=self.bg1, fg=self.fg1, font=self.font1,
                 text="Insert password length (integer)").pack()

        password_len = tk.Entry(newWindow, width=30)  # Entry widget for password length input
        password_len.pack()

        tk.Button(newWindow, command=lambda: self.new_entry_box(password_len.get()), height=2, width=12,
                  bg=self.bg2, fg=self.fg1, font=self.font1, text='Create password').pack()

        newWindow.mainloop()

    def new_entry_box(self, length):
        try:  # if length field not filled
            length = int(length)
            if length > 20 or length < 1:
                length = 15
        except:
            length = 12

        password = self.generate(length)  # function from Scripts, creates a random password
        self.generated.delete(0, 'end')
        self.generated.insert(0, str(password))
        self.generated.pack()  # packs the generated password Entry widget

    def m_list(self):
        newWin = tk.Toplevel()
        newWin.title('Different Choices')
        newWin.configure(bg=self.bg1)

        mic_list = self.mic_list()
        microphones = ''

        for i in mic_list:  # sorts all the inputs. NEEDS FURTHER WORK
            if i not in microphones and 'output' not in i.lower() and 'speakers' not in i.lower():
                microphones += i + '\n'

        microphone_list = tk.Label(newWin, bg=self.bg1, fg=self.fg1, text=microphones)  # displays the sorted list
        microphone_list.pack()