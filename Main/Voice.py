import speech_recognition as sr
import pyttsx3
from time import sleep
from Scripts import git_initter
from Scripts import google_search
from Scripts import gto
from Scripts import job_scraper
from Scripts import password_generator as pg


class Voice:
    def __init__(self, how_are_you1, how_are_you2, about_creators1, about_creators2):
        self.how_are_you1 = how_are_you1
        self.how_are_you2 = how_are_you2
        self.about_creators1 = about_creators1
        self.about_creators2 = about_creators2
        self.voice = sr.Recognizer()
        self.microphone = sr.Microphone()
        self.text_in = ""
        self.name = "jessica"

    def voice_in(self):
        with self.microphone as source:
            self.voice.adjust_for_ambient_noise(source)
            audio = self.voice.listen(source, phrase_time_limit=4)  # audio is from the microphone
            self.text_in = self.voice.recognize_google(audio)  # uses api to make speech-to-text
            print(self.text_in)

    @staticmethod
    def mic_list():
        microphone_list = sr.Microphone.list_microphone_names()
        return microphone_list

    @staticmethod
    def voice_output(text):
        engine = pyttsx3.init()
        engine.setProperty('rate', 125)
        engine.say(text)
        engine.runAndWait()

    def say_hello(self):
        if self.text_in.lower() == "hello " + self.name:
            self.voice_output("Hello User")
            pass

    def say_how_are_you(self):
        if self.text_in.lower() == "how are you " + self.name:
            self.voice_output(self.how_are_you1)
            sleep(0.3)
            self.voice_output(self.how_are_you2)

    def say_creators(self):
        if self.name + " who are your creators" in self.text_in.lower():
            sleep(0.05)
            self.voice_output(self.about_creators1)
            sleep(0.1)
            self.voice_output(self.about_creators2)

    def kill_app(self):
        if "have a nice day " + self.name in self.text_in.lower():
            self.voice_output("Okay user, it was fun. If I don’t see "
                              "you around, I'll see you square.")
            return True

    def open_google(self):
        if self.text_in.lower() == "open google":
            self.voice_output("I am opening Google")
            gto.google_tab_opener()

    def run_job_scraper(self):
        """
        Tartu: 314
        Tallinn: 312
        offset = x * 20 (next page)
        """
        city = ""
        try:
            url = None
            if self.text_in.lower() == "jessica show jobs in tartu":
                url = job_scraper.set_url("INFORMATION_TECHNOLOGY", "314")
                city = "Tartu"
            elif self.text_in.lower() == "jessica show jobs in tallinn":
                url = job_scraper.set_url("INFORMATION_TECHNOLOGY", "312")
                city = "Tallinn"
            elif self.text_in.lower() == "jessica show all jobs":
                url = "https://cv.ee/search?limit=20&offset=0&categories%5B0%5D=INFORMATION_TECHNOLOGY&isHourlySalary=false"
            else:
                offerings = None

            self.voice_loop(city)
            offerings = job_scraper.find_job_offerings(url)
            job_scraper.display_job_offerings(offerings)
        except:
            pass

    def voice_loop(self, city):
        if city == "":
            self.voice_output("Wait a second, let me look")
            sleep(2)
            self.voice_output("Oh yeah, I am fast. There are quite many offerings actually. I will display "
                              "them in exel")
        else:
            self.voice_output("Wait a second, let me look")
            sleep(2)
            self.voice_output(f"Oh yeah, I am fast. There are many offerings in {city}. I will display "
                              "them in exel")

    def search(self):
        google_text = ""
        if 'Google search' in self.text_in:
            g_search = self.text_in.split(' ')
            for i in g_search:
                if i == 'Google' and g_search[g_search.index(i) + 1] == 'search':
                    begins = g_search.index(i) + 2
                    g_search = g_search[begins:]
                    for i in g_search:
                        google_text += i + '+'
                    google_search.google_search_tab(google_text)
        return None

    def git_init(self):
        if "jessica i need a new project" in self.text_in.lower():
            git_initter.cmd_commands("new_project")
            self.voice_output("Git inited a new project in "
                              "your current user directory")

    @staticmethod
    def generate(length):
        return pg.random_password(length)